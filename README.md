## Angular2 & RoR Microservices

Repository: https://ptrboro@bitbucket.org/ptrboro/angular2-rails.git

Navigate to angular2 app folder:

```
cd path/to/angular-frontend
```

Install Angular2 modules:

```
npm install
```

Run angular2 server: 

```
npm start
```

Open app in your browser and enjoy :)

If it doesn't start wait 30 seconds and refresh the page.
It is using rails microservices hosted on heroku and sometimes it tooks up to 30 seconds for heroku to start. 

You can also run rails microservices on your own machine. They are in fr_docs and fr_proposals folders.
Remember to change URLs of microservices in: 
angular-frontend/app/documents/document.service.ts
and 
angular-frontend/app/proposal/proposal.service.ts