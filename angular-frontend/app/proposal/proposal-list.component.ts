import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ProposalService } from './proposal.service';
import { Proposal } from './proposal';

@Component({
    moduleId: module.id,
    selector: 'proposal-list',
    templateUrl: 'proposal-list.component.html',
    styleUrls: ['proposal-list.component.css'],
    providers: [ ProposalService ]
})
export class ProposalListComponent implements OnInit {
    proposals: Proposal[];
    mode = "Observable";
    errorMessage: string;
    
    constructor(
        private proposalService: ProposalService,
        private router: Router
        ) {}
        
    ngOnInit() {
        let timer = Observable.timer(0, 5000);
        timer.subscribe(() => this.getProposals());
    }
    
    getProposals() {
        this.proposalService.getProposals()
            .subscribe(
                proposals => this.proposals = proposals,
                error => this.errorMessage = <any>error
            );
    }
    goToShow(proposal: Proposal): void {
        let link = ['/proposals', proposal.id];
        this.router.navigate(link);
    }
}